#!/bin/bash

cwd=$(dirname "$0")
args=$*

source "$cwd/venv/bin/activate"

python "$cwd/FlatCAM.py" "$args"

deactivate
